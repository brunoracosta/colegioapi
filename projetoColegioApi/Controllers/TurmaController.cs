﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Data.Repositorio;
using Domain;
using Microsoft.AspNetCore.Mvc;

namespace projetoColegioApi.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class TurmaController : ControllerBase
    {

        [Route("{paramPage:int}/{paramPageSize:int}")]
        [HttpGet]
        public async Task<IEnumerable<Turma>> Get(
            int paramPage,
            int paramPageSize,
            [FromQuery] string filter,
            [FromQuery] string sortBy = "nome",
            [FromQuery] string sort = "asc"
        )
        {
            TurmaRepositorio repositorio = new TurmaRepositorio();
            return await repositorio.List(
                paramPage,
                paramPageSize,
                filter,
                sortBy,
                sort
            );
        }

        [Route("{paramId:int}")]
        [HttpGet]
        public async Task<ActionResult<Turma>> Get(int paramId)
        {
            TurmaRepositorio repositorio = new TurmaRepositorio();
            return await repositorio.BuscaByID(paramId);
        }

        [HttpPost]
        public async Task<ActionResult<Turma>> Create([FromBody] Turma paramTurma)
        {
            try
            {
                TurmaRepositorio repositorio = new TurmaRepositorio();
                IReadOnlyList<string> lst = paramTurma.IsValido();
                if (lst.Count == 0)
                {
                    return await repositorio.Criar(paramTurma);
                }

                return BadRequest(lst);
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }

        }

        [Route("{paramId:int}")]
        [HttpPut]
        public async Task<ActionResult<Turma>> Atualizar(
            int paramId,
            [FromBody] Turma paramTurma
        )
        {
            try
            {
                TurmaRepositorio repositorio = new TurmaRepositorio();
                if (paramTurma.id != paramId)
                {
                    return BadRequest();
                }

                IReadOnlyList<string> lst = paramTurma.IsValido();
                if (lst.Count == 0)
                {
                    return await repositorio.Update(paramTurma);
                }

                return BadRequest(lst);
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }

        }
    }
}
