﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Data.Repositorio;
using Domain;
using Microsoft.AspNetCore.Mvc;

namespace projetoColegioApi.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ColegioController : ControllerBase
    {

        [Route("{paramPage:int}/{paramPageSize:int}")]
        [HttpGet]
        public async Task<IEnumerable<Colegio>> Get(
            int paramPage,
            int paramPageSize,
            [FromQuery] string filter,
            [FromQuery] string sortBy = "nome",
            [FromQuery] string sort = "asc"
        )
        {
            ColegioRepositorio repositorio = new ColegioRepositorio();
            return await repositorio.List(
                paramPage,
                paramPageSize,
                filter,
                sortBy,
                sort
            );
        }

        [Route("{paramId:int}")]
        [HttpGet]
        public async Task<ActionResult<Colegio>> Get(int paramId)
        {
            ColegioRepositorio repositorio = new ColegioRepositorio();
            return await repositorio.BuscaByID(paramId);
        }

        [HttpPost]
        public async Task<ActionResult<Colegio>> Create([FromBody] Colegio paramColegio)
        {
            try
            {
                ColegioRepositorio repositorio = new ColegioRepositorio();
                IReadOnlyList<string> lst = paramColegio.IsValido();
                if (lst == null)
                {
                    //paramColegio.sanatizaCampos();
                    return await repositorio.Criar(paramColegio);
                }

                return BadRequest(lst);
            }
            catch (Exception ex)
            {
                var c = ex.Message;
                return BadRequest(ex);
            }

        }

        [Route("{paramId:int}")]
        [HttpPut]
        public async Task<ActionResult<Colegio>> Atualizar(
            int paramId,
            [FromBody] Colegio paramColegio
        )
        {
            try
            {
                ColegioRepositorio repositorio = new ColegioRepositorio();
                if (paramColegio.id != paramId)
                {
                    return BadRequest();
                }

                IReadOnlyList<string> lst = paramColegio.IsValido();
                if (lst == null)
                {
                    return await repositorio.Update(paramColegio);
                }

                return BadRequest(lst);
            }
            catch (Exception ex)
            {
                var c = ex.Message;
                return BadRequest(ex);
            }

        }
    }
}
