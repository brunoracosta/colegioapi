﻿using System;
using System.Collections.Generic;

namespace Domain
{
    public class Turma
    {
        public int id { get; set; }
        public int id_colegio { get; set; }
        public string nome { get; set; }

        public virtual Colegio colegio { get; set; }

        public virtual ICollection<TurmaPessoa> Alunos { get; set; }

        public IReadOnlyList<string> IsValido()
        {
            List<string> lst = new List<string>();
            if (String.IsNullOrWhiteSpace(nome))
            {
                lst.Add("Nome precisa ser preenchido");
            }
            if (id_colegio == 0)
            {
                lst.Add("Colégio precisa ser selecionado");
            }

            return lst;
        }
    }
}