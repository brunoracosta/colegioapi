﻿using System;

namespace Domain
{
    public class TurmaPessoa
    {
        public int id_turma { get; set; }
        public int id_aluno { get; set; }
        public DateTime? data_final { get; set; }
        public bool ativo { get; set; }

        public Aluno Aluno { get; set; }
        public Turma Turma { get; set; }
    }
}
