﻿using System;
using System.Collections.Generic;

namespace Domain
{
    public class Aluno
    {
        public int id { get; set; }
        public string nome { get; set; }
        public DateTime data_nascimento { get; set; }
        public string telefone { get; set; }
        public string cpf { get; set; }

        public virtual ICollection<TurmaPessoa> HistoritoTurmas { get; set; }
    }
}
