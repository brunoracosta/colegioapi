﻿using System;
using System.Collections.Generic;

namespace Domain
{
    public class Colegio
    {
        public int id { get; set; }
        public string nome { get; set; }
        public string endereco { get; set; }

        public virtual ICollection<Turma> Turmas { get; set; }

        public IReadOnlyList<String> IsValido()
        {
            if (String.IsNullOrWhiteSpace(nome))
            {
                return new List<string>
                {
                    "Nome precisa ser preenchido"
                };
            }

            return null;

        }
    }
}
