﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Domain;
using Microsoft.EntityFrameworkCore;

namespace Data
{
    public class Context : DbContext
    {

        private string connectionString = "Server = localhost; Database = projetocolegio; Trusted_Connection = false;User=sa;Password=reallyStrongPwd123 ";

        public DbSet<Colegio> Colegio { get; set; }

        public DbSet<Aluno> Aluno { get; set; }

        public DbSet<Turma> Turma { get; set; }

        public DbSet<TurmaPessoa> TurmaPessoa { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder
                    .UseSqlServer(connectionString, providerOptions => providerOptions.CommandTimeout(60))
                    .UseQueryTrackingBehavior(QueryTrackingBehavior.NoTracking);
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Colegio>()
                .HasKey(s => s.id);
            modelBuilder.Entity<Colegio>()
                .Property<DateTime>("Created");
            modelBuilder.Entity<Colegio>()
                .Property<DateTime>("LastModified");

            modelBuilder.Entity<Aluno>()
                .HasKey(s => s.id);
            modelBuilder.Entity<Aluno>()
                .Property(b => b.data_nascimento).HasColumnType("Date");
            modelBuilder.Entity<Aluno>()
                .Property<DateTime>("Created");
            modelBuilder.Entity<Aluno>()
                .Property<DateTime>("LastModified");


            modelBuilder.Entity<TurmaPessoa>()
                .HasKey(s => new { s.id_aluno, s.id_turma });
            modelBuilder.Entity<TurmaPessoa>()
                .Property<DateTime>("Created");
            modelBuilder.Entity<TurmaPessoa>()
                .Property<DateTime>("LastModified");
            modelBuilder.Entity<TurmaPessoa>()
                .Property("ativo")
                .HasDefaultValue(true);

            modelBuilder.Entity<TurmaPessoa>()
                .HasOne(a => a.Aluno)
                .WithMany(p => p.HistoritoTurmas)
                .HasForeignKey(pt => pt.id_aluno);

            modelBuilder.Entity<TurmaPessoa>()
                .HasOne(a => a.Turma)
                .WithMany(p => p.Alunos)
                .HasForeignKey(pt => pt.id_turma);


            modelBuilder.Entity<Turma>()
                .HasKey(s => s.id);

            modelBuilder.Entity<Turma>()
                .HasOne(pt => pt.colegio)
                .WithMany(p => p.Turmas)
                .HasForeignKey(pt => pt.id_colegio);

            modelBuilder.Entity<Turma>()
                .Property<DateTime>("Created");
            modelBuilder.Entity<Turma>()
                .Property<DateTime>("LastModified");

        }


        public override int SaveChanges()
        {
            ChangeTracker.DetectChanges();
            DateTime timestamp = DateTime.Now;
            foreach (var entry in ChangeTracker.Entries()
                .Where(e => (e.State == EntityState.Added || e.State == EntityState.Modified)
                             && !e.Metadata.IsOwned()))
            {
                entry.Property("LastModified").CurrentValue = timestamp;
                if (entry.State == EntityState.Added)
                {
                    entry.Property("Created").CurrentValue = timestamp;
                }
            }
            return base.SaveChanges();
        }

        public override Task<int> SaveChangesAsync(bool acceptAllChangesOnSuccess, CancellationToken cancellationToken = default)
        {
            ChangeTracker.DetectChanges();
            DateTime timestamp = DateTime.Now;
            foreach (var entry in ChangeTracker.Entries()
                .Where(e => (e.State == EntityState.Added || e.State == EntityState.Modified)
                             && !e.Metadata.IsOwned()))
            {
                entry.Property("LastModified").CurrentValue = timestamp;
                if (entry.State == EntityState.Added)
                {
                    entry.Property("Created").CurrentValue = timestamp;
                }
            }

            return base.SaveChangesAsync(acceptAllChangesOnSuccess, cancellationToken);
        }


        //public override int SaveChangesAsync()
        //{
        //    ChangeTracker.DetectChanges();
        //    DateTime timestamp = DateTime.Now;
        //    foreach (var entry in ChangeTracker.Entries()
        //        .Where(e => (e.State == EntityState.Added || e.State == EntityState.Modified)
        //                     && !e.Metadata.IsOwned()))
        //    {
        //        entry.Property("LastModified").CurrentValue = timestamp;
        //        if (entry.State == EntityState.Added)
        //        {
        //            entry.Property("Created").CurrentValue = timestamp;
        //        }
        //    }
        //    return base.SaveChangesAsync();
        //}
    }
}
