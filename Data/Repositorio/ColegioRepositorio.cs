﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Domain;

namespace Data.Repositorio
{
    public class ColegioRepositorio : BaseRepositorio<Colegio>
    {
        public override async Task<IReadOnlyList<Colegio>> List(
            int paramPage,
            int paramPageSize,
            string paramFilter,
            string paramSortBy,
            string paramSort
        )
        {
            using (Context context = new Context())
            {
                return await Task.Run(() =>
                {
                    IQueryable<Colegio> query = context.Colegio.AsQueryable();

                    if (!String.IsNullOrEmpty(paramFilter))
                    {
                        query.Where(a => a.nome.Contains(paramFilter));
                    }

                    query = OrderByCustom(query, paramSortBy, paramSort.Equals("asc"));

                    return query
                        .Skip(paramPage * paramPageSize)
                        .Take(paramPageSize)
                        .ToList();
                });
            }
        }
    }
}
