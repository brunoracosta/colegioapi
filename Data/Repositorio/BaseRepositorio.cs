﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Data.Repositorio
{
    public abstract class BaseRepositorio<TEntity> where TEntity : class
    {

        public IQueryable<TEntity> OrderByCustom(IQueryable<TEntity> source, string columnName, bool isAscending = true)
        {
            if (String.IsNullOrEmpty(columnName))
            {
                return source;
            }

            ParameterExpression parameter = Expression.Parameter(source.ElementType, "");

            MemberExpression property = Expression.Property(parameter, columnName);
            LambdaExpression lambda = Expression.Lambda(property, parameter);

            string methodName = isAscending ? "OrderBy" : "OrderByDescending";

            Expression methodCallExpression = Expression.Call(typeof(Queryable), methodName,
                                  new Type[] { source.ElementType, property.Type },
                                  source.Expression, Expression.Quote(lambda));

            return source.Provider.CreateQuery<TEntity>(methodCallExpression);
        }

        public async Task<TEntity> BuscaByID(int paramId)
        {
            using (Context context = new Context())
            {
                return await context.FindAsync<TEntity>(paramId);
            }
        }

        public abstract Task<IReadOnlyList<TEntity>> List(
            int paramPage,
            int paramPageSize,
            string paramFilter,
            string paramSortBy,
            string paramSort
        );

        public IReadOnlyCollection<TEntity> ListAll()
        {
            using (Context context = new Context())
            {
                return context.Set<TEntity>().ToList();
            }
        }

        public async Task<TEntity> Criar(TEntity paramEntiy)
        {
            using (Context context = new Context())
            {
                context.Add<TEntity>(paramEntiy);
                await context.SaveChangesAsync(true);
                return paramEntiy;
            }
        }

        public async Task<TEntity> Update(TEntity paramEntiy)
        {
            using (Context context = new Context())
            {
                context.Update<TEntity>(paramEntiy);
                await context.SaveChangesAsync(true);
                return paramEntiy;
            }
        }
    }
}
