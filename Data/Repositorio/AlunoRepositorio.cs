﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Domain;
using Microsoft.EntityFrameworkCore;

namespace Data.Repositorio
{
    public class AlunoRepositorio : BaseRepositorio<Aluno>
    {

        public override async Task<IReadOnlyList<Aluno>> List(
            int paramPage,
            int paramPageSize,
            string paramFilter,
            string paramSortBy,
            string paramSort
        )
        {
            using (Context context = new Context())
            {

                return await Task.Run(() =>
                {
                    IQueryable<Aluno> query = context.Aluno.AsQueryable();

                    if (!String.IsNullOrEmpty(paramFilter))
                    {
                        query.Where(a => a.nome.Contains(paramFilter));
                    }

                    query = OrderByCustom(query, paramSortBy, paramSort.Equals("asc"));

                    return query
                        .Skip(paramPage * paramPageSize)
                        .Take(paramPageSize)
                        .ToList();
                });
            }
        }

        public async Task<IReadOnlyList<Aluno>> ListByTurma(
            int paramPage,
            int paramPageSize,
            int paramTurmaId,
            string paramFilter,
            string paramSortBy,
            string paramSort
        )
        {
            using (Context context = new Context())
            {

                return await Task.Run(() =>
                {
                    IQueryable<Aluno> query = context.Aluno.AsQueryable();

                    if (!String.IsNullOrEmpty(paramFilter))
                    {
                        query = query.Where(a => a.nome.Contains(paramFilter));
                    }

                    query = query.Include(x => x.HistoritoTurmas.Where(ht => ht.ativo == true && ht.id_turma == paramTurmaId));

                    query = OrderByCustom(query, paramSortBy, paramSort.Equals("asc"));

                    return query
                        .Skip(paramPage * paramPageSize)
                        .Take(paramPageSize)
                        .ToList();
                });
            }
        }
    }
}
